package com.centennial.avencare
import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL


object Webservice {

    var callbackTest: (()->ArrayList<Task>)? = null

    var loggedNurse:Nurse = Nurse();

    //val baseURL = "http://18.225.10.83:3000/";

    val baseURL = "http://10.0.2.2:3001/";

    init {
    }

    var variableName = "I am Var"

    fun printVarName(): String {
        return variableName
    }

    fun loadPictureByID(id: String, context: Context, callback: (image: Bitmap) -> Unit) {


        val url = Webservice.baseURL + "pictures/" + id + ".png"


        val queue = Volley.newRequestQueue(context)
        val ir = ImageRequest(url,
            Response.Listener { response ->

                callback(response)

                //image.setImageBitmap(response)

            },
            0,
            0,
            Bitmap.Config.RGB_565,
            Response.ErrorListener { Log.e("AVENCARE", "Image Load Error: ") })

        queue.add(ir)

    }

    fun getAlarms(context: Context, callback: (alarms: ArrayList<Alarm>?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url =  baseURL + "alarms/"

        //Log.d("Webservice","getAlarmByNurseID: " + url )

        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->

                Log.d("Webservice","getAlarmsByNurseID response: " + response )

                if (response.isEmpty()) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                var strResponse = response.toString()

                val alarms : ArrayList<Alarm> = ArrayList()


                val jsonArray: JSONArray = JSONArray(strResponse)
                for (i in 0 until jsonArray.length()) {
                    var jsonInner: JSONObject = jsonArray.getJSONObject(i)
                    var alarm =  Alarm()
                    alarm.initWithJSON(jsonInner)
                    if (alarm.completed == false) {
                        alarms.add(alarm)

                    }

                }

                callback.invoke(alarms,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = error.messege
                callback.invoke(null,error)

            })

        queue.add(stringRequest)

    }

    fun createReport(report: Report, context: Context, callback: (success: Boolean?, error: Error?) -> Unit) {

        val queue = Volley.newRequestQueue(context)
        val url =  baseURL + "reports/"

        Log.d("Webservice","createReport: " + url )

        val jsonObject = report.toJSON()

        val jsonRequest = JsonObjectRequest(Request.Method.POST, url,jsonObject,
            Response.Listener { response ->


                if (response == null ) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                Log.d("Webservice","createReport response: " + response )



                callback.invoke(true,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = error.messege
                callback.invoke(null,error)

            })

        queue.add(jsonRequest)
    }


    fun updateAlarm(alarm: Alarm, context: Context, callback: (success: Boolean?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url =  baseURL + "alarms/" + alarm.id

        //Log.d("Webservice","getAlarmByNurseID: " + url )

        val jsonObject = alarm.toJSON()

        val jsonRequest = JsonObjectRequest(Request.Method.POST, url,jsonObject,
            Response.Listener { response ->

                //Log.d("Webservice","getAlarmsByNurseID response: " + response )

                if (response == null ) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                Log.d("Webservice","updateAlarm response: " + response )



                callback.invoke(true,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })

        queue.add(jsonRequest)

    }

    fun getNurseByID(id: String, context: Context, callback: (nurse: Nurse?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url =  baseURL + "nurses/" + id

        Log.d("Webservice","getNurseByID: " + url )



        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->

                Log.d("Webservice","response: " + response )

                if (response.isEmpty()) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }


                var strResponse = response.toString()
                val jsonObject: JSONObject = JSONObject(strResponse)
                var nurse =  Nurse()
                nurse.initWithJSON(jsonObject)
                println(jsonObject)
                callback.invoke(nurse,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })

        queue.add(stringRequest)

    }


    fun getNurseByRegistrationNumber(rn: String, context: Context, callback: (nurse: Nurse?, error: Error?) -> Unit){

        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "nurses/rn/" + rn

        Log.d("Webservice","getNurseByRegistrationNumber: " + url )

        val jsonRequest = JsonObjectRequest(Request.Method.GET, url,null,
            Response.Listener { response ->

                Log.d("Webservice","response: " + response )

                var nurse =  Nurse()
                nurse.initWithJSON(response)
                callback.invoke(nurse,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = error.messege
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }


    fun getPatientByID(id: String, context: Context, callback: (patient: Patient?, error: Error?) -> Unit){

        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "patients/" + id

        Log.d("Webservice","getPatientByID: " + url )

        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->

                Log.d("Webservice","response: " + response )

                if (response.isEmpty()) {
                    val error = Error()
                    error.messege = "getPatientByID: " + id + " get response 0 "
                    callback.invoke(null,error)
                    return@Listener
                }


                var strResponse = response.toString()
                val jsonObject: JSONObject = JSONObject(strResponse)
                var patient =  Patient()
                 patient.initWithJSON(jsonObject)
                 callback.invoke(patient,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })


        queue.add(stringRequest)

    }

    fun getTaskByID(id: String, context: Context, callback: (task: Task?, error: Error?) -> Unit){

        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "tasks/" + id

        Log.d("Webservice","getTaskByID: " + url )

        val jsonRequest = JsonObjectRequest(Request.Method.GET, url,null,
            Response.Listener { response ->

                Log.d("Webservice","response: " + response )

                var task =  Task()
                task.initWithJSON(response)
                callback.invoke(task,null)


            },
            Response.ErrorListener {error->
                val error = Error()
                error.messege = error.messege
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }

    fun searchReports(search: String, context: Context, callback: (result: ArrayList<Report>?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "reports/search/?search=" + search

        Log.d("Webservice","searchReports: " + url )


        val jsonRequest = JsonArrayRequest(Request.Method.GET, url,null,
            Response.Listener { response ->

                if (response == null) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                val reports : ArrayList<Report> = ArrayList()

                val jsonArray = response
                for (i in 0 until jsonArray.length()) {
                    var jsonInner: JSONObject = jsonArray.getJSONObject(i)
                    var report =  Report()
                    report.initWithJSON(jsonInner)
                    reports.add(report)

                }

                callback.invoke(reports,null)


            },
            Response.ErrorListener {error->
                Log.d("Webservice","Error getTasksFromNurse response: " + error )

                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }

    fun getAllReports(context: Context, callback: (reports: ArrayList<Report>?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "reports/"

        Log.d("Webservice","getAllReports: " + url )


        val jsonRequest = JsonArrayRequest(Request.Method.GET, url,null,
            Response.Listener { response ->
                //Log.d("Webservice","getAllReports response: " + response )

                if (response == null) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                val reports : ArrayList<Report> = ArrayList()

                val jsonArray = response
                for (i in 0 until jsonArray.length()) {
                    var jsonInner: JSONObject = jsonArray.getJSONObject(i)
                    var report =  Report()
                    report.initWithJSON(jsonInner)
                    reports.add(report)

                }

                callback.invoke(reports,null)


            },
            Response.ErrorListener {error->
                Log.d("Webservice","Error getTasksFromNurse response: " + error )

                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }

    fun searchPatients(search: String, context: Context, callback: (result: ArrayList<Patient>?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "patients/search/?search=" + search

        Log.d("Webservice","getAllPatients: " + url )


        val jsonRequest = JsonArrayRequest(Request.Method.GET, url,null,
            Response.Listener { response ->
                Log.d("Webservice","getAllPatients response: " + response )

                if (response == null) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                val patients : ArrayList<Patient> = ArrayList()

                val jsonArray = response
                for (i in 0 until jsonArray.length()) {
                    var jsonInner: JSONObject = jsonArray.getJSONObject(i)
                    var patient =  Patient()
                    patient.initWithJSON(jsonInner)
                    patients.add(patient)

                }

                callback.invoke(patients,null)


            },
            Response.ErrorListener {error->
                Log.d("Webservice","Error getTasksFromNurse response: " + error )

                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }




    fun getAllPatients(context: Context, callback: (result: ArrayList<Patient>?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "patients/"

        Log.d("Webservice","getAllPatients: " + url )


        val jsonRequest = JsonArrayRequest(Request.Method.GET, url,null,
            Response.Listener { response ->
                Log.d("Webservice","getAllPatients response: " + response )

                if (response == null) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                val patients : ArrayList<Patient> = ArrayList()

                val jsonArray = response
                for (i in 0 until jsonArray.length()) {
                    var jsonInner: JSONObject = jsonArray.getJSONObject(i)
                    var patient =  Patient()
                    patient.initWithJSON(jsonInner)
                    patients.add(patient)

                }

                callback.invoke(patients,null)


            },
            Response.ErrorListener {error->
                Log.d("Webservice","Error getTasksFromNurse response: " + error )

                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }

    fun getTasksFromNurse(id:String,context: Context, callback: (result: ArrayList<Task>?, error: Error?) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url = baseURL + "nurses/"+ id + "/tasks"

        Log.d("Webservice","getTasks: " + url )


        val jsonRequest = JsonArrayRequest(Request.Method.GET, url,null,
            Response.Listener { response ->
                Log.d("Webservice","getTasksFromNurse response: " + response )

                if (response == null) {
                    val error = Error()
                    callback.invoke(null,error)
                    return@Listener
                }

                val tasks : ArrayList<Task> = ArrayList()

                val jsonArray = response
                for (i in 0 until jsonArray.length()) {
                    var jsonInner: JSONObject = jsonArray.getJSONObject(i)
                    var task =  Task()
                    task.initWithJSON(jsonInner)
                    tasks.add(task)

                }

                callback.invoke(tasks,null)


            },
            Response.ErrorListener {error->
                Log.d("Webservice","Error getTasksFromNurse response: " + error )

                val error = Error()
                error.messege = "Error server side"
                callback.invoke(null,error)

            })


        queue.add(jsonRequest)

    }



    fun helloWorldAPI(context: Context){

        val url = URL("http://10.0.2.2:3000/nurses/5de839e765f02c13a87b33ef/tasks")

        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "GET"  // optional default is GET

            println("\nSent 'GET' request to URL : $url; Response Code : $responseCode")


        }



//        var request = object  : JsonObjectRequest(Request.Method.GET, url, )
//
//        var requestToRegisterUser = object : JsonObjectRequest(url,Request.Method.GET,
//            Response.Listener { response ->
//                //toast(response.toString())
//
//            },
//            Response.ErrorListener { error ->
//                //toast(error.toString())
//            })
//        {
//            override fun getHeaders() : Map<String,String> {
//
//            }
//        }

//        val stringRequest = StringRequest(
//            Request.Method.GET, url,
//            Response.Listener<String> { response ->
//                // Display the first 500 characters of the response string.
//               // textViewMain.text = response//"Response is: ${response.substring(0, 500)}"
//
//
//                val json = JSONObject(response)
//
//
//
//                //Log.d("WebserviceAPI","JSON is: ${json}")
//
//
//            },
//
//            Response.ErrorListener {error ->  Log.d("WebserviceAPI",error.localizedMessage) })
//
//            queue.add(stringRequest)


    }

}