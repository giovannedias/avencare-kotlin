package com.centennial.avencare

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import java.security.Timestamp


class Report {


    var open = false



    var resultFields : ArrayList<Field> = ArrayList()


    var id: String = ""
    var nurse_id: String = ""
    var patient_id: String = ""
    var task_id: String = ""
    var timestamp: String = ""

    var note: String = ""



    var patient: Patient = Patient()
    var nurse: Nurse = Nurse()
    var task: Task = Task()

    var readings : ArrayList<Reading> = ArrayList()



    init {

    }

    fun initWith(taskID: String, nurseID: String, patientID: String) {

        this.task_id = taskID
        this.nurse_id = nurseID
        this.patient_id = patientID
        this.timestamp = System.currentTimeMillis().toString()

    }

    fun toJSON(): JSONObject {

        val params = HashMap<Any?,Any?>()

        params["nurse_id"] = this.nurse_id
        params["timestamp"] = this.timestamp
        params["patient_id"] = this.patient_id
        params["task_id"] = this.task_id
        params["note"] = this.note

        val jsonArray = JSONArray()

        for (field in resultFields) {
            jsonArray.put(field.toJSON())
        }

        params["readings"] = jsonArray


        val jsonObject = JSONObject(params)

        return jsonObject

    }


    fun initWithJSON(json: JSONObject) {

       // Log.d("Avencare","Report JSON: " + json)




         this.id = json.getString("_id")
         this.task_id = json.optString("task_id")
         this.nurse_id = json.optString("nurse_id")
         this.patient_id = json.optString("patient_id")


        this.patient.initWithReportJSON(json.optJSONObject("patient"))
        this.nurse.initWithReportJSON(json.optJSONObject("nurse"))
        this.task.initWithReportJSON(json.optJSONObject("nurse"))

        val jsonReadings = json.optJSONArray("readings")

        for (i in 0 until jsonReadings.length()) {
            var jsonInner: JSONObject = jsonReadings.getJSONObject(i)
            val reading = Reading()
            reading.initWithJSON(jsonInner)
            readings.add(reading)

        }



        Log.d("Avencare","Pa tient nameReport JSON: " + this.patient.first_name)



    }


    fun viewLayout(externalContext: Context, callback: (report: Report) -> Unit): View {

        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(externalContext)
        val header: View


        header = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_report_header, null)


        val dateReport = header.findViewById(R.id.dateReport) as TextView
        dateReport.text = task.title

        val idReport = header.findViewById(R.id.idReport) as TextView
        idReport.text = id

        val categoryReport = header.findViewById(R.id.categoryReport) as TextView
        categoryReport.text = task.category




        val patientNameReport = header.findViewById(R.id.patientNameReport) as TextView
        patientNameReport.text = patient.first_name + " " + patient.last_name

        val patientRoomReport = header.findViewById(R.id.patientRoomReport) as TextView
        patientRoomReport.text = "Room: " + patient.room


        val nurseNameReport = header.findViewById(R.id.nurseNameReport) as TextView
        nurseNameReport.text = nurse.first_name + " " + nurse.last_name

        val nurseIDReport = header.findViewById(R.id.nurseIDReport) as TextView
        nurseIDReport.text = nurse.register_nurse_id

        val patientProfileReport = header.findViewById(com.centennial.avencare.R.id.patientProfileReport) as ImageView

        Webservice.loadPictureByID(patient_id,externalContext, { bitmap->

            patientProfileReport.setImageBitmap(bitmap)
        })

        val nurseProfileReport = header.findViewById(com.centennial.avencare.R.id.nurseProfileReport) as ImageView

        Webservice.loadPictureByID(nurse_id,externalContext, { bitmap->

            nurseProfileReport.setImageBitmap(bitmap)
        })



        val moreInfoPlaceholder = header.findViewById(R.id.moreInfoPlaceholder) as LinearLayout



        val moreInfo = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_report, null)

        moreInfoPlaceholder.addView(moreInfo)


        val allReadingsPlaceholder = moreInfo.findViewById(R.id.allReadingsPlaceholder) as LinearLayout





        header.setOnClickListener {

            if (open) {
                allReadingsPlaceholder.removeAllViews()
                open = false

            } else {


                Log.d("Avencare","Load all resultings")
                for (reading in readings) {

                    val readingLayout = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_report_reading, null)

                    val resultTitle = readingLayout.findViewById(R.id.resultTitle) as TextView
                    resultTitle.text = reading.title

                    val resultValue = readingLayout.findViewById(R.id.resultValue) as TextView
                    resultValue.text = reading.result

                    allReadingsPlaceholder.addView(readingLayout)

                }



                open = true

            }


            callback(this)

        }

        return header
    }

}

