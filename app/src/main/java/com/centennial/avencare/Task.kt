package com.centennial.avencare

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import java.io.Serializable

 class Task(){


    var id: String = ""
    var nurse_id : String = ""
    var patient_id : String = ""
    var timestamp : String = ""
    var duration : String = ""
    var category : String = ""
    var time_to_start : String = ""
    var completed : Boolean = false
    var note : String = ""
    var title : String = ""


    var patient: Patient = Patient()
    var nurse: Nurse = Nurse()


    init {

    }

    fun initWithJSON(json: JSONObject) {

        this.id = json.getString("_id")
        this.category = json.optString("category")
        this.nurse_id = json.optString("nurse_id")
        this.patient_id = json.optString("patient_id")
        this.timestamp = json.optString("timestamp")
        this.duration = json.optString("duration")
        this.time_to_start = json.optString("time_to_start")
        this.completed = json.optBoolean("completed")
        this.note = json.optString("note")
        this.title = json.optString("title")
        this.patient.initWithReportJSON(json.optJSONObject("patient"))
        this.nurse.initWithReportJSON(json.optJSONObject("nurse"))


        //this.patient.initWithJSON(json.optJSONObject("patient"))
        //this.nurse.initWithJSON(json.optJSONObject("nurse"))


    }

     fun initWithReportJSON(json: JSONObject) {

         this.category = json.optString("category")
         this.timestamp = json.optString("timestamp")
         this.duration = json.optString("duration")
        // this.time_to_start = json.optString("time_to_start")
         this.completed = json.optBoolean("completed")
         this.note = json.optString("note")
         this.title = json.optString("title")


         //this.patient.initWithJSON(json.optJSONObject("patient"))
         //this.nurse.initWithJSON(json.optJSONObject("nurse"))


     }





    fun description(): String {

        return "Task category: " + category +
                " Patient: " + patient.last_name + "," +
                patient.first_name +
                " Room: " + patient.room  +
                " Nurse: " +  nurse.first_name
    }

    fun viewLayout(full: Boolean, context: Context, callback: (task: Task) -> Unit): View{


        val mainListlayoutInflater: LayoutInflater = LayoutInflater.from(context)
        val viewTask: View

        viewTask = mainListlayoutInflater.inflate(R.layout.layout_task_header, null)

        val titleTaskTextView = viewTask.findViewById(R.id.titleTaskTextView) as TextView
        titleTaskTextView.text = time_to_start + " " + title

        val durationTaskTextView = viewTask.findViewById(R.id.durationTaskTextView) as TextView
        durationTaskTextView.text = duration

        val namePatientTaskTaskTextView = viewTask.findViewById(R.id.namePatientTaskTaskTextView) as TextView
        namePatientTaskTaskTextView.text = patient.first_name + " " + patient.last_name



        val patientTaskImageView = viewTask.findViewById(com.centennial.avencare.R.id.patientTaskImageView) as ImageView

        Webservice.loadPictureByID(patient_id,context, { bitmap->

            patientTaskImageView.setImageBitmap(bitmap)
        })



        Log.d("Avencare","Get pic by id: " + patient_id)

        val roomTaskTextView = viewTask.findViewById(R.id.roomTaskTextView) as TextView
        roomTaskTextView.text = "Room: " + patient.room



        viewTask.setOnClickListener {


            callback(this)

        }

        if (full) {
            val moreInfoPlaceholder = viewTask.findViewById(R.id.moreInfoPlaceholder) as LinearLayout

            val viewMoreInfoTask: View

            viewMoreInfoTask = mainListlayoutInflater.inflate(R.layout.layout_task, null)

            val notesTestView = viewMoreInfoTask.findViewById(R.id.notesTestView) as TextView
            notesTestView.text = "Notes: " + note



            moreInfoPlaceholder.addView(viewMoreInfoTask)


            val startStopButton =  viewMoreInfoTask.findViewById(R.id.startStopButton) as Button
            startStopButton.setOnClickListener {
                if (startStopButton.text == "START") {
                    startStopButton.setText("PAUSE")
                } else {
                    startStopButton.setText("START")
                }
            }





        }

        return viewTask
    }


}


