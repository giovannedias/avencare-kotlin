package com.centennial.avencare

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_new_report.*
import java.security.Timestamp

class NewReportActivity : AppCompatActivity() {

    lateinit var addNewReportFieldButton : Button
    lateinit var getNextReportFieldButton : Button
    lateinit var getPrevReportFieldButton : Button


    var resultFields : ArrayList<Field> = ArrayList()
    var newField = Field()
    var resultFieldsPlaceholder : LinearLayout? = null

    var isEditing = false

    var task = Task()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_report)


        val toolbar = findViewById(R.id.toolBar) as Toolbar
        val backButton = toolbar.findViewById(R.id.backButton) as Button
        backButton.visibility = View.INVISIBLE

        val menuButton = toolbar.findViewById(R.id.menudButton) as Button
        menuButton.visibility = View.INVISIBLE

        setSupportActionBar(toolbar)


        resultFieldsPlaceholder = findViewById(R.id.resultFieldsPlaceholder) as LinearLayout
        addNewReportFieldButton = findViewById(R.id.addNewReportFieldButton) as Button
        getNextReportFieldButton  = findViewById(R.id.getNextReportFieldButton) as Button
        getPrevReportFieldButton  = findViewById(R.id.getPrevReportFieldButton) as Button

        //Instantiating object with  object from first activity
        val taskID = intent.getSerializableExtra("TaskID") as String

        Webservice.getTaskByID(taskID,this){taskFounded, error ->

            if (taskFounded != null) {
                task = taskFounded
                loadViews()
            }
        }

        val saveButton  = findViewById(R.id.saveButton) as Button

        saveButton.setOnClickListener {

           val newReport = Report()
            newReport.initWith(task.id,task.nurse_id,task.patient_id)
            newReport.resultFields = resultFields

            Log.d("Avencare", "Report JSON: " + newReport.toJSON())

            Webservice.createReport(newReport,this){success, error ->

                if ( success != null) {

                    Log.d("Avencare","New report created")

                    Toast.makeText(this,"New Report was created!",Toast.LENGTH_SHORT)

                        val intent = Intent(this@NewReportActivity, HomeActivity::class.java)

                        startActivity(intent)



                }


            }


        }



    }

    fun loadViews(){
        val newFieldPlaceholder = findViewById(R.id.newFieldPlaceholder) as LinearLayout

        val newFieldReadingView = newField.getNewReadingLayout(this)

        newFieldPlaceholder.addView(newFieldReadingView)


        addNewReportFieldButton.setOnClickListener {

            val field = Field(newField.title,newField.value,newField.unit,System.currentTimeMillis().toString(),newField.type)
            resultFields.add(field)


            reloadListView()
            newField.cleanFields()
            addNewReportFieldButton.setText("Add")


        }



        getNextReportFieldButton.setOnClickListener {

            newFieldPlaceholder.removeAllViews()
            if (newField.type == FieldType.READING) {
                val newFieldSlider = newField.getNewSliderLayout(this)
                newFieldPlaceholder.addView(newFieldSlider)
                return@setOnClickListener
            }

            if (newField.type == FieldType.SLIDER) {
                val newBooleanField = newField.getNewBooleanLayout(this)
                newFieldPlaceholder.addView(newBooleanField)
                return@setOnClickListener

            }

            if (newField.type == FieldType.BOOLEAN) {
                val newFieldReadingView = newField.getNewReadingLayout(this)
                newFieldPlaceholder.addView(newFieldReadingView)
                return@setOnClickListener

            }

        }

        getPrevReportFieldButton.setOnClickListener {
            newFieldPlaceholder.removeAllViews()

            newFieldPlaceholder.removeAllViews()
            if (newField.type == FieldType.READING) {
                val newBooleanField = newField.getNewBooleanLayout(this)
                newFieldPlaceholder.addView(newBooleanField)
                return@setOnClickListener

            }

            if (newField.type == FieldType.SLIDER) {
                val newFieldReadingView = newField.getNewReadingLayout(this)
                newFieldPlaceholder.addView(newFieldReadingView)
                return@setOnClickListener

            }

            if (newField.type == FieldType.BOOLEAN) {

                val newFieldSlider = newField.getNewSliderLayout(this)
                newFieldPlaceholder.addView(newFieldSlider)
                return@setOnClickListener


            }

        }


    }

    fun setFieldViewByType(type: FieldType) {
        newFieldPlaceholder.removeAllViews()

        if (type == FieldType.BOOLEAN) {
            val newBooleanField = newField.getNewBooleanLayout(this)
            newFieldPlaceholder.addView(newBooleanField)
            return

        }

        if (type == FieldType.READING) {
            val newFieldReadingView = newField.getNewReadingLayout(this)
            newFieldPlaceholder.addView(newFieldReadingView)
            return

        }

        if (type == FieldType.SLIDER) {
            val newFieldSlider = newField.getNewSliderLayout(this)
            newFieldPlaceholder.addView(newFieldSlider)
            return

        }
    }


    fun reloadListView(){
        resultFieldsPlaceholder?.let {
            it.removeAllViews()
        }

        val animation = AnimationUtils.loadAnimation(this, R.anim.slide_up)


        for (field in resultFields) {
            Log.d("Avencare","Field category: " + field.type)
            val resultView = field.getResultLayout(this, {
                //remove
                resultFields.remove(it)
                reloadListView()

            },{
                //edit

                setFieldViewByType(it.type)
                newField.edit(it)
                resultFields.remove(it)
                addNewReportFieldButton.setText("Update")
                isEditing = true

            })


            resultFieldsPlaceholder?.let {
                it.addView(resultView)
                resultView.startAnimation(animation)

            }

        }
    }
}


