package com.centennial.avencare

import android.util.Log
import org.json.JSONObject


class Reading {

    var title: String = ""
    var result: String = ""

    init {

    }

    fun initWithJSON(json: JSONObject) {

         //Log.d("Avencare","Reading Report JSON: " + json)


        this.title = json.getString("name")
        this.result = json.optString("result")



    }




}