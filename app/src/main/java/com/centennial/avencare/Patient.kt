package com.centennial.avencare

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONObject
import kotlin.Error


class Patient {

    var open = false

    var id: String = ""
    var first_name : String = ""
    var last_name : String = ""
    var date_of_birth : String = ""
    var sex : String? = null
    var height : String = ""
    var weight : String = ""
    var address : String = ""
    var city : String = ""
    var zip_code : String = ""
    var emergency_contact : String = ""
    var emergency_contact_relationship : String = ""
    var emergency_contact_number : String = ""
    var email : String = ""
    var room : String = ""
    var note : String = ""
    var condition : String = ""


    init {

    }

    fun initWithJSON(json: JSONObject) {


            this.id = json.getString("_id")
            this.first_name = json.optString("first_name")
            this.last_name = json.optString("last_name")
            this.date_of_birth = json.optString("date_of_birth")
            this.sex = json.optString("sex")
            this.height = json.optString("height")
            this.weight = json.optString("weight")
            this.address = json.optString("address")
            this.note = json.optString("note")
            this.city = json.optString("city")
            this.zip_code = json.optString("zip_code")
            this.emergency_contact = json.optString("emergency_contact")
            this.emergency_contact_number = json.optString("emergency_contact_number")
            this.emergency_contact_relationship = json.optString("emergency_contact_relationship")
            this.email = json.optString("email")
            this.room = json.optString("room")
            this.note = json.optString("note")


    }

    fun initWithReportJSON(json: JSONObject) {


        this.first_name = json.optString("first_name")
        this.last_name = json.optString("last_name")
        this.date_of_birth = json.optString("date_of_birth")
        this.sex = json.optString("sex")
        this.height = json.optString("height")
        this.weight = json.optString("weight")
        this.note = json.optString("note")

        this.emergency_contact = json.optString("emergency_contact")
        this.emergency_contact_number = json.optString("emergency_contact_number")
        this.emergency_contact_relationship = json.optString("emergency_contact_relationship")
        this.room = json.optString("room")
        this.note = json.optString("note")

        this.condition = json.optString("condition")







    }

    fun viewLayout(externalContext: Context, callback: (patient: Patient) -> Unit): View {

        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(externalContext)
        val view: View


        view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_patient_header, null)


        val patientName = view.findViewById(R.id.patientName) as TextView
        patientName.text = first_name + " " + last_name


        val patientRoom = view.findViewById(R.id.patientRoom) as TextView
        patientRoom.text = "Room: " + room


        val patientProfilePicture = view.findViewById(com.centennial.avencare.R.id.patientProfilePicture) as ImageView

        Webservice.loadPictureByID(id,externalContext, { bitmap->

            patientProfilePicture.setImageBitmap(bitmap)
        })







        val placeholder = view.findViewById<LinearLayout>(R.id.placeholder)

        view.setOnClickListener {

            if (open) {
                placeholder.removeAllViews()
                open = false

            } else {
                val moreInfo = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_patient, null)


                val patientHeight = moreInfo.findViewById(R.id.patientHeight) as TextView
                patientHeight.text = height


                val patientWeight = moreInfo.findViewById(R.id.patientWeight) as TextView
                patientWeight.text = weight

                val patientSex = moreInfo.findViewById(R.id.patientSex) as TextView
                patientSex.text = sex

                val patientEmargencyContact = moreInfo.findViewById(R.id.patientEmargencyContact) as TextView
                patientEmargencyContact.text = emergency_contact

                val patientEmargencyContactStatus = moreInfo.findViewById(R.id.patientEmargencyContactStatus) as TextView
                patientEmargencyContactStatus.text = emergency_contact_relationship

                val patientEmargencyContactNumber = moreInfo.findViewById(R.id.patientEmargencyContactNumber) as TextView
                patientEmargencyContactNumber.text = emergency_contact_number

                val patientEmail = moreInfo.findViewById(R.id.patientEmail) as TextView
                patientEmail.text = email

                val patientAddress = moreInfo.findViewById(R.id.patientAddress) as TextView
                patientAddress.text = address


                val patientCity = moreInfo.findViewById(R.id.patientCity) as TextView
                patientCity.text = city

                val patientZipCode = moreInfo.findViewById(R.id.patientZipCode) as TextView
                patientZipCode.text = zip_code


                placeholder.addView(moreInfo)



                open = true

            }


            callback(this)

        }

        return view
    }

}


