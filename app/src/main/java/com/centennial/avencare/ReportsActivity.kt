package com.centennial.avencare

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar

class ReportsActivity : AppCompatActivity() {

    lateinit var allReportsPlaceholder : LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports)

        val toolbar = findViewById(R.id.toolBar) as Toolbar
        val backButton = toolbar.findViewById(R.id.backButton) as Button
        backButton.visibility = View.VISIBLE


        backButton.setOnClickListener {
            val intent = Intent(this@ReportsActivity, HomeActivity::class.java)

            startActivity(intent)

        }

        val menuButton = toolbar.findViewById(R.id.menudButton) as Button
        menuButton.visibility = View.INVISIBLE

        setSupportActionBar(toolbar)
        allReportsPlaceholder = findViewById(R.id.allReportsPlaceholder) as LinearLayout


        getall()

        val searchView = findViewById(R.id.searchView) as LinearLayout


        val editTextSearch = searchView.findViewById(R.id.editTextSearch) as EditText

        editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                val search = p0.toString()

                if (search == "") {
                    getall()

                } else {
                    searchPatients(search)

                }

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare", "Text: " + p0)


            }
        })




    }

    fun searchPatients(search: String) {

        allReportsPlaceholder.removeAllViews()


        Webservice.searchReports(search,this){ reports: ArrayList<Report>?, error: Error? ->

            if (reports!=null) {
                for (report in reports) {

                    val view = report.viewLayout(this){report ->

                    }
                    allReportsPlaceholder.addView(view)
                }

            }
        }

    }

    fun getall(){

        allReportsPlaceholder.removeAllViews()
        Webservice.getAllReports(this){ reports: ArrayList<Report>?, error: Error? ->

            if (reports!=null) {
                for (report in reports) {

                    val view = report.viewLayout(this){report ->

                    }
                    allReportsPlaceholder.addView(view)
                }

            }
        }

    }
}
