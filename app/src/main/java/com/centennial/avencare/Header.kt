package com.centennial.avencare

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.R
import com.android.volley.VolleyError
import android.graphics.Bitmap
import android.view.ContextMenu
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.animation.AnimationUtils
import java.io.Serializable
import com.android.volley.toolbox.Volley
import com.android.volley.RequestQueue


enum class StatusNurse {
    BUSY, AVAILABLE, BREAK
}


class Header: Serializable, View.OnClickListener {


    var nurseStatus : StatusNurse = StatusNurse.AVAILABLE

    lateinit var busyButton : Button
    lateinit var availableButton : Button
    lateinit var breakButton : Button

    lateinit var backgroundBrek : LinearLayout
    lateinit var backgroundAvailable : LinearLayout
    lateinit var backgroundBusy : LinearLayout

    var flagSmall = false


    var context : Context? = null

    init {

    }

    public var viewLayout : LinearLayout? = null


    public fun setupView(view: LinearLayout, externalContext: Context) {

        context = externalContext

        if (view != null){


            val name = view.findViewById(com.centennial.avencare.R.id.nameTextView) as TextView
            val rn = view.findViewById(com.centennial.avencare.R.id.rnTextView) as TextView
            val profilePicture = view.findViewById(com.centennial.avencare.R.id.profilePicture) as ImageView


            name.setText(Webservice.loggedNurse.first_name + " " +Webservice.loggedNurse.last_name )
            rn.setText(Webservice.loggedNurse.register_nurse_id)

            loadPicture(externalContext,profilePicture)

        }

    }

    fun loadPicture(context: Context, image :ImageView) {


        val url = Webservice.baseURL + "pictures/" + Webservice.loggedNurse.id + ".png"


        if (CachedImage.image != null) {
            image.setImageBitmap(CachedImage.image)
            return
        }

        val queue = Volley.newRequestQueue(context)
        val ir = ImageRequest(url,
            Response.Listener { response ->
                CachedImage.image = response
                image.setImageBitmap(response) },
            0,
            0,
            Bitmap.Config.RGB_565,
            Response.ErrorListener { Log.e("AVENCARE", "Image Load Error: ") })

        queue.add(ir)

    }

    fun viewLayout(fullSize: Boolean, externalContext: Context, callback: (status: Int) -> Unit): View{

        context = externalContext

        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(context)
        val view: View

        if (fullSize) {

            view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_header, null)

        } else {

            view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_small_header, null)


            if (flagSmall) {
               // backgroundBrek = view.findViewById(com.centennial.avencare.R.id.backgroundBrek) as LinearLayout
               // backgroundAvailable = view.findViewById(com.centennial.avencare.R.id.backgroundAvailable) as LinearLayout
               // backgroundBusy = view.findViewById(com.centennial.avencare.R.id.backgroundBusy) as LinearLayout
            }




        }

        busyButton = view.findViewById(com.centennial.avencare.R.id.busyButton) as Button
        availableButton = view.findViewById(com.centennial.avencare.R.id.availableButton) as Button
        breakButton = view.findViewById(com.centennial.avencare.R.id.breakButton) as Button

        busyButton.setOnClickListener(this)
        availableButton.setOnClickListener(this)
        breakButton.setOnClickListener(this)




        checkStatus()


        val profilePicture = view.findViewById(com.centennial.avencare.R.id.profilePicture) as ImageView

        loadPicture(externalContext,profilePicture)

        view.setOnClickListener {


            callback(1)

        }

        return view
    }

    fun checkStatus(){

        busyButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)
        availableButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)
        breakButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)




        if (nurseStatus == StatusNurse.BREAK) {
            breakButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

        }

        if (nurseStatus == StatusNurse.AVAILABLE) {
            availableButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

        }

        if (nurseStatus == StatusNurse.BUSY) {
            busyButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

        }

        if (flagSmall) {


           // backgroundBusy.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)
           // backgroundAvailable.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)
           // backgroundBrek.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)



        }

        if (flagSmall) {


           /* if (nurseStatus == StatusNurse.BREAK) {
                backgroundBusy.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

            }

            if (nurseStatus == StatusNurse.AVAILABLE) {
                backgroundAvailable.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

            }

            if (nurseStatus == StatusNurse.BUSY) {
                backgroundBrek.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

            }*/
        }


        }


    override fun onClick(v: View?){

        busyButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)
        availableButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)
        breakButton.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_normal)


        val button = v as Button
        button.setBackgroundResource(com.centennial.avencare.R.drawable.button_background_selected)

        if (button == busyButton) {
            nurseStatus =StatusNurse.BUSY


            return
        }
        if (button == availableButton) {
            nurseStatus =StatusNurse.AVAILABLE



            return
        }
        if (button == breakButton) {
            nurseStatus =StatusNurse.BREAK



            return
        }




    }


}

object CachedImage {
    public var image : Bitmap? = null

}