package com.centennial.avencare

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import org.json.JSONObject
import java.security.Timestamp


enum class FieldType {
    READING, SLIDER, BOOLEAN
}


class Field  {


    var type:FieldType = FieldType.READING

    var id : String = ""

    var title : String = ""
    var value : String = ""
    var unit : String = ""

    constructor()

    constructor(title: String, value:String, unit: String, timestamp: String, type: FieldType) {
        this.title = title
        this.value = value
        this.unit = unit
        this.id = timestamp
        this.type = type
    }

    lateinit var newReadingTitle : EditText
    lateinit var newReadingValue : EditText
    lateinit var newReadingUnit : EditText

    fun getNewReadingLayout(externalContext: Context): View {

        type = FieldType.READING

        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(externalContext)
        val view: View


        view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_new_report_field_reading, null)
        newReadingTitle = view.findViewById(R.id.newReadingTitle) as EditText
        newReadingValue = view.findViewById(R.id.newReadingValue) as EditText
        newReadingUnit = view.findViewById(R.id.newReadingUnit) as EditText

        newReadingTitle.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                title = p0.toString()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare","Text: " + p0 )


            }
        })

        newReadingValue.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                value = p0.toString()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare","Text: " + p0 )


            }
        })

        newReadingUnit.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                unit = p0.toString()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare","Text: " + p0 )

            }
        })



        return view
    }

    lateinit var slider : SeekBar
    lateinit var percent : TextView

    fun getNewSliderLayout(externalContext: Context): View {

        type = FieldType.SLIDER


        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(externalContext)
        val view: View


        view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_new_report_field_slider, null)
        newReadingTitle = view.findViewById(R.id.newReadingTitle) as EditText
        slider = view.findViewById(R.id.seekBar) as SeekBar
        percent = view.findViewById(R.id.percentField) as TextView

        value = "30"
        unit = "%"

        slider.setOnSeekBarChangeListener(object :SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                // Display the current progress of SeekBar
                percent.text = " $i %"

                value = "$i"

            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // Do something
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                // Do something
            }
        })

        newReadingTitle.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                title = p0.toString()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare","Text: " + p0 )


            }
        })




        return view
    }

    lateinit var switchForm :Switch


    fun getNewBooleanLayout(externalContext: Context): View {

        type = FieldType.BOOLEAN


        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(externalContext)
        val view: View


        view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_new_report_field_boolean, null)
        newReadingTitle = view.findViewById(R.id.newReadingTitle) as EditText
        switchForm = view.findViewById(R.id.switchForm) as Switch

        value = "No"

        switchForm.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                // The switch is enabled/checked
               value = "Yes"
            } else {
                // The switch is disabled
                value = "No"

            }
        }

        newReadingTitle.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                title = p0.toString()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare","Text: " + p0 )


            }
        })




        return view
    }

    fun edit(field: Field) {
        newReadingTitle.setText(field.title)
        newReadingValue.setText(field.value)
        newReadingUnit.setText(field.unit)

        if (type == FieldType.SLIDER) {

            percent.setText(field.value + " %")
            slider.setProgress(field.value.toInt())

        }

        if (type == FieldType.BOOLEAN) {
            if (value == "No") {
                switchForm.isChecked = false
            } else {
                switchForm.isChecked = true

            }
        }


    }



    fun cleanFields(){

        newReadingTitle.setText("")
        newReadingValue.setText("")
        newReadingUnit.setText("")
        id = ""

        if (::percent.isInitialized) {
            percent.setText("30%")
        }


    }

    fun update(field: Field) {

        this.apply {
            title = field.title
        }

        Log.d("Avencare", "new title should be: " + field.title + " but it is " + this.title)

        this.value = field.value
        this.unit = field.unit
        this.type = field.type
    }
    fun checkField(){

    }

    lateinit var resultTitle : TextView
    lateinit var resultValue : TextView
    lateinit var resultUnit : TextView


    fun getResultLayout(externalContext: Context,remove: (field: Field) -> Unit, edit: (field: Field) -> Unit): View {

        val mainListLayoutInflater: LayoutInflater = LayoutInflater.from(externalContext)
        val view: View


        view = mainListLayoutInflater.inflate(com.centennial.avencare.R.layout.layout_report_field_result, null)
        resultTitle = view.findViewById(R.id.resultTitle) as TextView
        resultValue = view.findViewById(R.id.resultValue) as TextView
        resultUnit = view.findViewById(R.id.resultUnit) as TextView

        val removeFieldButton =  view.findViewById(R.id.removeFieldButton) as Button
        removeFieldButton.setOnClickListener {
            remove(this)
        }

        val editFieldButton =  view.findViewById(R.id.editFieldButton) as Button
        editFieldButton.setOnClickListener {
            edit(this)
        }


        resultTitle.setText(title)
        resultValue.setText(value)
        resultUnit.setText(unit)


        return view
    }

    fun toJSON(): JSONObject {

        val params = HashMap<Any?, Any?>()

        params["name"] = this.title
        params["result"] = this.value + " " + this.unit

        val jsonObject = JSONObject(params)

        return jsonObject


    }
}