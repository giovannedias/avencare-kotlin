package com.centennial.avencare

import org.json.JSONObject

class Nurse {

    var id: String = ""
    var first_name : String = ""
    var last_name : String = ""
    var register_nurse_id : String = ""
    var shift : String = ""
    var admin : Boolean = false
    var date_of_birth : String = ""
    var address : String = ""
    var city : String = ""
    var zip_code : String = ""
    var number : String = ""
    var email : String = ""


    init {

    }

    fun initWithJSON(json: JSONObject) {

        this.id = json.getString("_id")
        this.first_name = json.optString("first_name")
        this.last_name = json.optString("last_name")
        this.register_nurse_id = json.optString("register_nurse_id")
        this.shift = json.optString("shift")
        this.admin = json.optBoolean("admin")
        this.date_of_birth = json.optString("date_of_birth")
        this.address = json.optString("address")
        this.city = json.optString("city")
        this.zip_code = json.optString("zip_code")
        this.number = json.optString("number")
        this.email = json.optString("email")

    }

    fun initWithReportJSON(json: JSONObject) {

        this.first_name = json.optString("first_name")
        this.last_name = json.optString("last_name")
        this.register_nurse_id = json.optString("register_nurse_id")
        this.shift = json.optString("shift")
        this.admin = json.optBoolean("admin")


    }




}