package com.centennial.avencare

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar

class PatientsActivity : AppCompatActivity() {


    lateinit var allPatientsPlaceholder : LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patients)

        val toolbar = findViewById(R.id.toolBar) as Toolbar
        val backButton = toolbar.findViewById(R.id.backButton) as Button
        backButton.visibility = View.VISIBLE

        val menuButton = toolbar.findViewById(R.id.menudButton) as Button
        menuButton.visibility = View.INVISIBLE

        backButton.setOnClickListener {
            val intent = Intent(this@PatientsActivity, HomeActivity::class.java)

            startActivity(intent)

        }

        setSupportActionBar(toolbar)

        allPatientsPlaceholder = findViewById(R.id.allPatientsPlaceholder) as LinearLayout



        getall()

        val searchView = findViewById(R.id.searchView) as LinearLayout


        val editTextSearch = searchView.findViewById(R.id.editTextSearch) as EditText

        editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                val search = p0.toString()

                if (search == "") {
                    getall()

                } else {
                    searchPatients(search)

                }

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("Avencare","Text: " + p0 )


            }
        })







    }

    fun searchPatients(search: String) {

        allPatientsPlaceholder.removeAllViews()


        Webservice.searchPatients(search,this){ patients: ArrayList<Patient>?, error: Error? ->

            if (patients!=null) {
                for (patient in patients) {

                    val view = patient.viewLayout(this){patient ->

                    }
                    allPatientsPlaceholder.addView(view)
                }

            }
        }


    }


    fun getall(){

        allPatientsPlaceholder.removeAllViews()


        Webservice.getAllPatients(this){ patients: ArrayList<Patient>?, error: Error? ->

            if (patients!=null) {
                for (patient in patients) {

                    val view = patient.viewLayout(this){patient ->

                    }
                    allPatientsPlaceholder.addView(view)
                }

            }
        }


    }
}
