package com.centennial.avencare

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_task.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlin.random.Random
import android.content.SharedPreferences
import android.R.id.edit
import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.R.layout
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.widget.*
import android.view.animation.Animation
import android.view.animation.ScaleAnimation




class HomeActivity : AppCompatActivity(), View.OnScrollChangeListener {


    lateinit var mainListLinerLayout : LinearLayout

    val mainHandler = Handler(Looper.getMainLooper())

    var playAlarm: MediaPlayer = MediaPlayer()

    var showingAlarm = false

    var allDailyTasks : ArrayList<Task> = ArrayList()


    lateinit var headerPlaceholder : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val toolbar = findViewById(R.id.toolBar) as Toolbar
        val backButton = toolbar.findViewById(R.id.backButton) as Button
        backButton.visibility = View.INVISIBLE

        val menuButton = toolbar.findViewById(R.id.menudButton) as Button
        menuButton.visibility = View.VISIBLE

        setSupportActionBar(toolbar)


        mainListLinerLayout = findViewById(R.id.linearLayout) as LinearLayout

        headerPlaceholder = findViewById(R.id.headerPlaceholder) as LinearLayout

        addHeader(true)



        playAlarm = MediaPlayer.create(this@HomeActivity, R.raw.alarm)

        this.loadDailyTasks()

        this.clean()

        mainHandler.post(object : Runnable {
            override fun run() {
                Log.d("Avencare","loop" )
                checkAlarms()


                mainHandler.postDelayed(this, 5000)
            }
        })

//        mainHandler.post(object : Runnable {
//            override fun run() {
//                Log.d("Avencare","loop" )
//               // checkAlarms()
//
//                loadDailyTasks()
//
//                mainHandler.postDelayed(this, 10000)
//            }
//        })




    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    //Men Optionsu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {




        when (item.itemId) {
            R.id.itemPatients-> {
                val intent = Intent(this@HomeActivity, PatientsActivity::class.java)

                startActivity(intent)
            }


            R.id.itemReports -> {
                val intent = Intent(this@HomeActivity, ReportsActivity::class.java)

                startActivity(intent)
            }

            /*R.id.itemUpdateInfo ->*/

            R.id.itemLogout -> {
                val intent = Intent(this@HomeActivity, LoginActivity::class.java)

                startActivity(intent)
            }


            else -> {
            }
        }



        return super.onOptionsItemSelected(item)
    }


    override fun onScrollChange(p0: View?, p1: Int, p2: Int, p3: Int, p4: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        Log.d("Avencare","scroooll " + p1.toString())

    }


    fun addHeader(fullSize:Boolean) {
        headerPlaceholder.removeAllViews()


        val headerSmall = (Header().viewLayout(false,this) {
            Log.d("header","addHeader false")
            addHeader(true)

        })


        val headerFull = (Header().viewLayout(true,this){
            Log.d("header","addHeader true")

            addHeader(false)
        })

        val slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up)
        val scaleUp = AnimationUtils.loadAnimation(this, R.anim.scale_up)
        val scaleDown = AnimationUtils.loadAnimation(this, R.anim.scale_down)
        val fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out)


        if (fullSize) {
            Log.d("header","addview fadeOut")

            headerPlaceholder.addView(headerFull)

        } else {

            Log.d("header","addview fadeOut")

            headerPlaceholder.addView(headerSmall)

        }
    }





    fun loadDailyTasks(){


        this.allDailyTasks = ArrayList()

        Webservice.getTasksFromNurse(Webservice.loggedNurse.id,this){ tasks: ArrayList<Task>?, error: Error? ->

            if ( error != null) { this.showError(error) }

            if (tasks != null) {
                this.allDailyTasks = tasks
            }
            this.showDailyTasks()
        }
    }


    fun showDailyTasks(){

        Log.d("Avencare","showDailyTasks " )

        mainListLinerLayout.removeAllViews()


        for(task in this.allDailyTasks) {

            Log.d("Avencare","showDailyTasks: " + task.description() )


            val view = (task.viewLayout(false,this) {
                val intent = Intent(this@HomeActivity, FullTaskActivity::class.java)
                intent.putExtra("TaskID",it.id)

                startActivity(intent)
            })

            view.setOnScrollChangeListener(this);

            mainListLinerLayout.addView(view)

            val animation = AnimationUtils.loadAnimation(this, R.anim.slide_up)
            view.startAnimation(animation)

        }
    }


    fun checkAlarms(){
        Webservice.getAlarms(this){ alarms: ArrayList<Alarm>?, error: Error? ->
            if ( error != null) { this.showError(error) }
            if (alarms != null) {
                for (alarm in alarms) {
                        this.showAlarm(alarm)
                }
            }

        }
    }

    fun removeAlarmView(alarm: Alarm) {

        Log.d("Avencare","Remove view ID: " + alarm.viewID)


        val childCount = alarmLayout.getChildCount()
        for (i in 0 until childCount) {
            val view = alarmLayout.getChildAt(i)
            if (view.id == alarm.viewID) {
                alarmLayout.removeViewAt(i);
            }
        }

    }
    fun showAlarm(alarm: Alarm){


        if (this.showingAlarm == true) { return }

        if (this.checkAlarmIsIgnored(alarm) == true) { return }
        this.showingAlarm = true

        playAlarm.start()

        alarmLayout.addView(alarm.viewLayout(this){ alarm ->
            Log.d("Avencare","Alarm dismiss callback: ")

            alarm.completed = true

            Webservice.updateAlarm(alarm,this){success, error ->

                if ( error != null) { this.showError(error) }

            }

            this.removeAlarmView(alarm)
            this.showingAlarm = false
            //this.ignoreAlarm(alarm.viewID)
            playAlarm.pause()


        })


        /*val mainListlayoutInflater: LayoutInflater = LayoutInflater.from(applicationContext)

        val viewAlarm: View

        var id = 2

        if (!alarm.timestamp.isEmpty()) {

            id = alarm.timestamp.toInt()
        }
        viewAlarm = mainListlayoutInflater.inflate(R.layout.layout_alarm, null)
        viewAlarm.id = id
        //val toolbar = findViewById(R.id.toolBar) as Toolbar
        val alarmLayout = findViewById(R.id.alarmLayout) as LinearLayout

        val dissmissButton = viewAlarm.findViewById(R.id.alarmDismissButton) as Button

        dissmissButton.id = id

        dissmissButton.setOnClickListener {

            val alarmLayout = findViewById(R.id.alarmLayout) as LinearLayout

            val childCount = alarmLayout.getChildCount()
            for (i in 0 until childCount) {
                val view = alarmLayout.getChildAt(i)
                if (view.id == it.id) {
                    alarmLayout.removeViewAt(i);
                    this.showingAlarm = false

                    this.ignoreAlarm(view.id.toString())

                }
            }

        }


        alarmLayout.addView(viewAlarm)
        this.showingAlarm = true*/



    }

    fun showError(error: Error) {

        Log.d("Avencare","Error: " + error.messege )

    }

    fun ignoreAlarm(alarmViewID: Int) {

        val context = this
        val sharedPref = context.getSharedPreferences(
            "Avencare", Context.MODE_PRIVATE

        )

        with (sharedPref.edit()) {
            Log.d("Avencare Alarm","Save Alarm ID: " + alarmViewID )

            putInt(alarmViewID.toString(), alarmViewID)
            commit()
        }


    }


    fun clean(){


        val sharedPref = this.getSharedPreferences(
            "Avencare", Context.MODE_PRIVATE
        )

        val map = sharedPref.all

        for (value in map) {
            sharedPref.edit().remove(value.key).commit();
        }

    }
    fun checkAlarmIsIgnored(alarm: Alarm) : Boolean {
        Log.d("Avencare Alarm","checkAlarmIsIgnored" )

        Log.d("Avencare Alarm","check new alarm" + alarm.viewID )

        val sharedPref = this.getSharedPreferences(
            "Avencare", Context.MODE_PRIVATE

        )
        val map = sharedPref.all


        for (value in map) {
            Log.d("Avencare Alarm","* Check Alarm ID: " + value.value )
            Log.d("Avencare Alarm","* compare to : " + alarm.viewID  )

        }

        for (value in map) {
            Log.d("Avencare Alarm","Check Alarm ID: " + value.value )
            Log.d("Avencare Alarm","compare to : " + alarm.viewID  )

            if (alarm.viewID == value.value) {

                Log.d("Avencare Alarm","trueeeeeeee" )


                return true
            }


        }

        if (this.showingAlarm == true) {
            return true
        }

        return false

    }




}


