package com.centennial.avencare

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.Toolbar

class FullTaskActivity : AppCompatActivity() {

    lateinit var startStopButton : Button

    var task = Task()

    var start : Boolean = false

    lateinit var headerPlaceholder : LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_task)

        val toolbar = findViewById(R.id.toolBar) as Toolbar
        val backButton = toolbar.findViewById(R.id.backButton) as Button
        backButton.visibility = View.VISIBLE

        val menuButton = toolbar.findViewById(R.id.menudButton) as Button
        menuButton.visibility = View.INVISIBLE

        backButton.setOnClickListener {
            val intent = Intent(this@FullTaskActivity, HomeActivity::class.java)

            startActivity(intent)

        }

        setSupportActionBar(toolbar)

        headerPlaceholder = findViewById(R.id.headerPlaceholder) as LinearLayout

        addHeader(true)


        //Instantiating object with  object from first activity
        val taskID = intent.getSerializableExtra("TaskID") as String

        Webservice.getTaskByID(taskID,this){taskFounded, error ->

            if (taskFounded != null) {
                task = taskFounded
                loadView()
            }
        }





    }

    fun loadView(){
        val taskViewPlaceholder = findViewById(R.id.taskViewPlaceholder) as LinearLayout

        val taskViewFull = task.viewLayout(true,this){


        }

        taskViewPlaceholder.addView(taskViewFull)
    }

    fun addHeader(fullSize:Boolean) {
        headerPlaceholder.removeAllViews()


        val headerSmall = (Header().viewLayout(false,this) {
            Log.d("header","addHeader false")
            addHeader(true)

        })


        val headerFull = (Header().viewLayout(true,this){
            Log.d("header","addHeader true")

            addHeader(false)
        })

        val slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up)
        val scaleUp = AnimationUtils.loadAnimation(this, R.anim.scale_up)
        val scaleDown = AnimationUtils.loadAnimation(this, R.anim.scale_down)
        val fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out)


        if (fullSize) {
            Log.d("header","addview fadeOut")

            headerPlaceholder.addView(headerFull)

        } else {

            Log.d("header","addview fadeOut")

            headerPlaceholder.addView(headerSmall)

        }
    }



    fun startStopTask(v: View?){

        if (this.start == false) {
            this.start = true
            this.startStopButton.setText("PAUSE")
        } else {
            this.start = false
            this.startStopButton.setText("START")
        }

    }

    fun finishTask(v: View?){

        Toast.makeText(this,"clicked",Toast.LENGTH_SHORT)

        //update task

        val intent = Intent(this@FullTaskActivity, NewReportActivity::class.java)
        intent.putExtra("TaskID",task.id)
        startActivity(intent)
    }
}

