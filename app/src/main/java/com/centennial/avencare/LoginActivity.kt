package com.centennial.avencare

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar


/*


nurse status color
nurse status online
task new web
daily tasks
anytime tasks
progress tasks
 */

class LoginActivity : AppCompatActivity() {


    lateinit var login : EditText
    lateinit var password : EditText

    lateinit var loginBackground : View
    lateinit var passwordBackground : View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val toolbar = findViewById(R.id.toolBar) as Toolbar
        val backButton = toolbar.findViewById(R.id.backButton) as Button
        backButton.visibility = View.INVISIBLE

        val menuButton = toolbar.findViewById(R.id.menudButton) as Button
        menuButton.visibility = View.INVISIBLE

        setSupportActionBar(toolbar)


        login = findViewById(R.id.loginEditText) as EditText
        password = findViewById(R.id.passwordEditText) as EditText

        loginBackground = findViewById(R.id.loginBackground) as View
        passwordBackground = findViewById(R.id.passwordBackground) as View

        login.setText("RN100123123")



        val loginButton = findViewById(R.id.loginButton) as Button

        loginButton.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {


                /*if (login.text.isEmpty()){
                    setErrorField(loginBackground)

                }*/

                if (password.text.isEmpty()){
                    setErrorField(passwordBackground)
                    return

                }

                Webservice.getNurseByRegistrationNumber(login.text.toString(), this@LoginActivity){nurse: Nurse?, error: Error? ->
                    if ( error != null) { showError(error) }

                    if (nurse!=null){
                        Webservice.loggedNurse = nurse

                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                    }

                }


            }
        })
    }

    fun showError(error: Error) {

        Log.d("Avencare","Error: " + error.messege )

    }

    fun setErrorField(view : View?) {

        if (view != null) {
            view.setBackgroundResource(R.drawable.background_error)

        }

    }
}
