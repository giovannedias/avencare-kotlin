package com.centennial.avencare

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONObject

class Alarm {

    var id: String = "1"
    var nurse_id : String = ""
    var allNurses : Boolean = false
    var timestamp : String = ""
    var message : String = ""
    var completed : Boolean = false
    var title : String = ""

    var viewID : Int = 0





    init {

    }

    fun initWithJSON(json: JSONObject) {


        //Log.d("Webservice","Alarm json: " + json )


        this.id = json.getString("_id")
        this.nurse_id = json.optString("nurse_id")
        this.timestamp = json.optString("timestamp")
        this.completed = json.optBoolean("completed")
        this.allNurses = json.optBoolean("allNurses")
        this.title = json.optString("title")
        this.message = json.optString("message")

        if (this.timestamp != null) {
            this.viewID =  this.timestamp.toInt()
        }

        Log.d("Webservice","Alarm viewID: " + this.viewID )




    }

    fun description(): String {

        return "Alarm: " + title + " Message: " + message
    }

    fun toJSON():JSONObject {

        val params = HashMap<Any?,Any?>()
        params["_id"] = this.id
        params["nurse_id"] = this.nurse_id
        params["timestamp"] = this.timestamp
        params["completed"] = this.completed
        params["allNurses"] = this.allNurses
        params["title"] = this.title
        params["message"] = this.message

        val jsonObject = JSONObject(params)

        return jsonObject

    }

    fun viewLayout(context: Context, callback: (alarm: Alarm) -> Unit): View {


        //this.viewID = this.timestamp.toInt()//(0..100).random()

        val mainListlayoutInflater: LayoutInflater = LayoutInflater.from(context)

        val viewAlarm: View

        viewAlarm = mainListlayoutInflater.inflate(R.layout.layout_alarm, null)

        viewAlarm.id = this.viewID


        val alarmTitleTextView = viewAlarm.findViewById(R.id.alarmTitleTextView) as TextView

        val alarmMessageTextView = viewAlarm.findViewById(R.id.alarmMessageTextView) as TextView

        val dissmissButton = viewAlarm.findViewById(R.id.alarmDismissButton) as Button

        alarmTitleTextView.text = this.title
        alarmMessageTextView.text = this.message

        dissmissButton.setOnClickListener {


            Log.d("Avencare","Alarm dismiss setOnClickListener: ")

            callback(this)
        }


        return viewAlarm


    }

}
